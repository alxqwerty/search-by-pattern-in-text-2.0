using System;

namespace SearchByPatternInText
{
    public static class Searcher
    {
        public static int[] SearchPatternString(this string text, string pattern, bool overlap)
        {
            if (text is null)
            {
                throw new ArgumentException("Text is null.", nameof(text));
            }

            if (pattern is null)
            {
                throw new ArgumentException("Pattern is null.", nameof(pattern));
            }

            int[] positions = Array.Empty<int>();

            int counterOfElements = 0;

            char[] convertedPattern = pattern.ToUpperInvariant().ToCharArray();
            char[] convertedText = text.ToUpperInvariant().ToCharArray();

            for (int i = 0, j = 0; i < text.Length; i++)
            {
                if (convertedText[i] == convertedPattern[j] && text.Length - i >= pattern.Length)
                {
                    for (int k = i + 1, z = j + 1; z < pattern.Length; k++, z++)
                    {
                        if (convertedText[k] == convertedPattern[z] && z == pattern.Length - 1)
                        {
                            int container = i + 1;
                            counterOfElements++;

                            int[] tempArray = new int[counterOfElements];
                            Array.Copy(positions, tempArray, tempArray.Length - 1);

                            tempArray[^1] = container;
                            positions = tempArray;

                            if (!overlap)
                            {
                                i = k;
                            }

                            break;
                        }
                        else if (convertedText[k] != convertedPattern[z])
                        {
                            break;
                        }
                    }
                }
                else if (text.Length - i < pattern.Length)
                {
                    break;
                }
            }

            return positions;
        }
    }
}